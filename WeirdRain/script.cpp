#include <deque>

#include "stdafx.h"
#include "inc/main.h"

#include "script.h"

std::deque<Vehicle> vehicles;

Vector3 Vec3Around(Vector3 vec, float radius)
{
	Vector3 newVec;

	newVec.x = vec.x + (float)(std::rand() % (int)(radius * 2) - radius);
	newVec.y = vec.y + (float)(std::rand() % (int)(radius * 2) - radius);
	newVec.z = vec.z;

	return newVec;
}

Ped GetPlayerPed()
{
	if (PLAYER::DOES_MAIN_PLAYER_EXIST())
	{
		return PLAYER::GET_PLAYER_PED(PLAYER::GET_PLAYER_ID());
	}

	return NULL;
}

void DeleteVeh(Vehicle veh)
{
	if (VEHICLE::DOES_VEHICLE_EXIST(veh))
	{
		Hash model = VEHICLE::GET_VEHICLE_MODEL(veh);

		VEHICLE::DELETE_VEHICLE(&veh);
		STREAMING::SET_MODEL_AS_NO_LONGER_NEEDED(model);
	}
}

void ClearVehicles()
{
	if (vehicles.size() > 0)
	{
		for (int i = 0; i < vehicles.size(); i++)
		{
			DeleteVeh(vehicles[i]);
		}

		vehicles.clear();
	}
}

void main()
{
	bool enabled = GetPrivateProfileInt(L"SETTINGS", L"ENABLED_BY_DEFAULT", 0, L".\\WeirdRain.ini");
	int toggleKey = GetPrivateProfileInt(L"SETTINGS", L"TOGGLE_KEY", VK_F9, L".\\WeirdRain.ini");
	int radius = GetPrivateProfileInt(L"SETTINGS", L"RADIUS", 15, L".\\WeirdRain.ini");

	int lastSpawnVeh = 0;

	while (true)
	{
		if (IsKeyJustUp(toggleKey))
		{
			enabled = !enabled;

			const char *printStr;
			if (enabled) printStr = "WeirdRain enabled";
			else printStr = "WeirdRain disabled";

			HUD::PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", printStr, 1000, 1);

			if (!enabled)
			{
				ClearVehicles();
			}
		}

		if (enabled)
		{
			Ped plrPed = GetPlayerPed();

			if (plrPed != NULL && PED::DOES_PED_EXIST(plrPed))
			{
				if (PED::IS_PED_DEAD(plrPed))
				{
					ClearVehicles();
				}
				else
				{
					int gameTime = MISC::GET_GAME_TIMER();

					if (gameTime - lastSpawnVeh >= 2500)
					{
						if (vehicles.size() >= 5)
						{
							DeleteVeh(vehicles[0]);

							vehicles.pop_front();
						}

						Vector3 plrPedPos = PED::GET_PED_COORDS(plrPed);
						int rand = std::rand() % (sizeof(vehicleNames) / sizeof(vehicleNames[0]));
						Hash vehHash = MISC::GET_HASH_KEY(vehicleNames[rand].c_str());

						for (int i = 0; i < 100; i++)
						{
							if (STREAMING::HAS_MODEL_LOADED(vehHash)) break;

							STREAMING::REQUEST_MODEL(vehHash);
							scriptWait(1);
						}

						if (STREAMING::HAS_MODEL_LOADED(vehHash))
						{
							Vector3 around = Vec3Around(plrPedPos, radius);
							Vehicle veh = VEHICLE::CREATE_VEHICLE(vehHash, around.x, around.y, around.z + 40.0f, 0.0f, true, false);

							if (veh != NULL && VEHICLE::DOES_VEHICLE_EXIST(veh))
							{
								VEHICLE::APPLY_FORCE_TO_VEHICLE(veh, 2, 0.0f, 0.0f, -50.0f, 5.0f, 5.0f, 5.0f, 0, false, false, false);
								vehicles.push_back(veh);
							}
						}

						lastSpawnVeh = gameTime;
					}
				}
			}
		}

		scriptWait(0);
	}
}

void ScriptMain()
{
	srand(GetTickCount());
	main();
}
